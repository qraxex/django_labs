from django.apps import AppConfig


class SamuelpaulyilascrumyConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'samuelpaulyilascrumy'
